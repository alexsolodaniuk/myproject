//Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
//При вызове функция должна спросить у вызывающего имя и фамилию.
//Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
//Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, 
//соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
//Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin(). 
//Вывести в консоль результат выполнения функции.
//Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.
//Возьмите выполненное задание выше (созданная вами функция createNewUser()) и дополните ее 
//следующим функционалом:
//При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и 
//сохранить ее в поле birthday.
//Создать метод getAge() который будет возвращать сколько пользователю лет.
//Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, 
//соединенную с фамилией (в нижнем регистре) и годом рождения. 
//(например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
//Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() 
//созданного объекта.
class CreateNewUser {
    constructor(name, surname, birthday) {
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;

    }
    getLogin() {
        console.log(`${this.name[0]}${this.surname}`.toLowerCase())
    }
    setLogin() {
        this.name = prompt('Введите имя');
        this.surname = prompt('Введите фамилию');
        this.birthday = prompt('Введите дату своего рождения', 'Формат: 00.00.0000');
    }
    getAge() {
        console.log(2021 - (this.birthday[6] + this.birthday[7] + this.birthday[8] + this.birthday[9]));
    }
    getPassword() {
        console.log(`${this.name[0]}`.toUpperCase() + `${this.surname}`.toLowerCase() + (this.birthday[6] + this.birthday[7] + this.birthday[8] + this.birthday[9]));
    }
}

let newUser = new CreateNewUser()
newUser.setLogin()
newUser.getLogin()
newUser.getAge()
newUser.getPassword()
