//создаем кнопку
const button = document.createElement('button');
//создаем константу с именем див для создания круга
const div = document.createElement('div');
//даем имя первой кнопке
button.innerText = 'Нарисовать круг';
//выводим первую кнопку на страницу
document.body.append(button);
//создаем константу второй кнопки
const button2 = document.createElement('button');
//создаем функцию для первой кнопки которая вызывает окно для ввода диаметра круга и выводит на страницу вторую кнопку
button.onclick = function () {
    //убираем первую кнопку из страницы после вызова функции
    button.style.display = 'none';
    //создаем окно для ввода ширины 
    div.style.width = prompt('Введите диаметр круга:', 'Например: 10') + 'px';
    //выводим на экран вторую кнопку
    document.body.append(button2);
    //даем имя второй кнопке
    button2.innerText = 'Нарисовать';
    //создаем функцию для кнопки после нажатия на которую начинаеться цыкл
    button2.onclick = function () {
        //создаем цыкл для вывода на страницу кругов и присвоения ширины, высоты, и скругленя рамки 
        for (i = 1; i < 100; i++) {
            div[i] = document.createElement('div');
            div[i].className = 'circle';
            div[i].style.width = div.style.width;
            div[i].style.height = div.style.width;
            div[i].style.borderRadius = div.style.width;
            //создаем функцию которая будет присваивать рандомный цвет кругу
            div[i].style.backgroundColor = '#' + Math.floor(Math.random() * 1000000);
            //навешиваем событие чтобы при нажатии на див он пропадал а соседний див смещался на его место
            div[i].onmousedown = function () {
                this.style.display = 'none';
            }
            //навешиваем событии при навидении меняеться вид курсора
            div[i].onmouseover = function () {
                this.style.cursor = 'pointer';
            }
            //выводим на экран круги 
            document.body.append(div[i])
        }
        //убираем вторую кнопку после вызова функции
        button2.style.display = 'none';

    }
}







