//Создайте многострочное поле для ввода текста и кнопку. После нажатия кнопки пользователем 
//приложение должно сгенерировать тег div с текстом который был в многострочном поле. многострочное 
//поле следует очистить после переимещени информации
const textarea = document.createElement('textarea');
const button = document.createElement('button');
const div = document.createElement('div');
button.innerText = 'Нажать кнопку';
document.body.append(textarea)
document.body.append(button)
button.onclick = function(){
    div.innerHTML = textarea.value;
    textarea.value = '';
    document.body.append(div);
}