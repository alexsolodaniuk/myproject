const array = [1, 2, 3, 4];
function fn(element) {
    return element * array.length;
}
function map(callback, newArray) {
    newArray.forEach(element => console.log(callback(element)));
}
map(fn, array);