function Human(name, surname, from, age) {
    this.allHuman = [
        {
            name: name,
            surname: surname,
            from: from,
            age: age,

        },
    ];

}
const vasilii = new Human('Vasilii', 'Kaidash', 'Kyiv', 22);
console.log(vasilii);
const tolya = new Human('Tolya', 'Borys', 'Poltava', 30);
console.log(tolya);
console.log(new Human());

//Human.prototype.getAge = function () {
//    //создаем функцию которая сортирует обьекты с age по возростанию
//    return this.allHuman.sort(function (a, b) {
//        return a.age - b.age;
//    });
//};
//const sortHuman = new Human();
//console.log(sortHuman.getAge());